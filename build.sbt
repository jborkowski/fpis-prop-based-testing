name := "fpis-prop-based-testing"

version := "1.0"

scalaVersion := "2.12.1"

organization := "com.jobo"

scalaVersion := "2.12.1"

libraryDependencies ++= Seq(
  "org.scalactic" %% "scalactic" % "3.0.0",
  "org.scalatest" %% "scalatest" % "3.0.0" % "test",
  "org.scalacheck" %% "scalacheck" % "1.13.4",
  "org.scalaz" %% "scalaz-core" % "7.2.8",
  "joda-time" % "joda-time" % "2.9.9",
  "org.joda" % "joda-convert" % "1.8.1"
)


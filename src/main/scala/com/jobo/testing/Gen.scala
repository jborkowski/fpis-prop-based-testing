package com.jobo.testing

import java.util.concurrent.{ExecutorService, Executors}

import com.jobo.testing.Prop._
import com.jobo.testing.Gen._
import com.jobo.Par._
import com.jobo.{Par, RNG, State}

case class Prop(run: (MaxSize,TestCases,RNG) => Result) {
  def &&(p: Prop): Prop = Prop {
    case (ms,tc, rng) => run(ms, tc, rng) match {
      case Passed | Proved => p.run(ms, tc, rng)
      case x => x
    }
  }

  def ||(p: Prop): Prop = Prop {
    case (ms, tc, rng) => run(ms, tc, rng) match {
      case Falsified(msg, _) => p.tag(msg).run(ms, tc, rng)
      case x => x
    }
  }

  def tag(msg: String): Prop = Prop {
    case (ms, tc, rng) => run(ms, tc, rng) match {
      case Falsified(errMsg, c) => Falsified(msg + " - " + errMsg, c)
      case x => x
    }
  }
}

object Prop {
  type FailedCase = String
  type SuccessCount = Int
  type TestCases = Int
  type MaxSize = Int

  sealed trait Result {
    def isFalsified: Boolean
  }
  case object Passed extends Result {
    override def isFalsified = false
  }
  case class Falsified(failedCase: FailedCase,
                       successCount: SuccessCount) extends Result {
    override def isFalsified = true
  }
  case object Proved extends Result {
    override def isFalsified: Boolean = false
  }

  def forAll[A](g: SGen[A])(f: A => Boolean): Prop =
    forAll(g(_))(f)

  def forAll[A](gen: Int => Gen[A])(f: A => Boolean): Prop = Prop {
    (max, n, rng) =>
      val casesPerSize = (n + (max -1)) / max
      val props: Stream[Prop] =
        Stream.from(0).take((n min max) + 1).map(i => forAll(gen(i))(f))
      val prop: Prop =
        props.map(p => Prop { (max, _, rng) =>
          p.run(max, casesPerSize, rng)
        }).toList.reduce(_ && _)
      prop.run(max, n, rng)
  }

  def forAll[A](gen: Gen[A])(f: A => Boolean): Prop = Prop {
    (_, n, rng) => randomStream(gen)(rng).zip(Stream.from(0)).take(n).map {
      case (a, i) => try {
        if (f(a)) Passed else Falsified(a.toString, i)
      } catch { case e: Exception => Falsified(buildMsg(a, e), i) }
    }.find(_.isFalsified).getOrElse(Passed)
  }

  def randomStream[A](g: Gen[A])(rng: RNG): Stream[A] =
    unfold(rng)(rng => Some(g.sample.run(rng)))

  def unfold[A, S](z: S)(f: S => Option[(A,S)]): Stream[A] =
    f(z) match {
      case Some((h,s)) => Stream.cons(h, unfold(s)(f))
      case None => Stream.empty
    }

  def buildMsg[A](s: A, e: Exception): String = {
    s"test case $s\n"
    s"generated an exception: ${e.getMessage}\n"
    s"stack trace:\n ${e.getStackTrace.mkString("\n")}"
  }

  def run(p: Prop,
          maxSize: Int = 100,
          testCases: Int = 100,
          rng: RNG = RNG.Simple(System.currentTimeMillis)): Unit =
    p.run(maxSize, testCases, rng) match {
      case Falsified(msg, n) =>
        println(s"! Falsified after $n passed test:\n $msg")
      case Passed =>
        println(s"+ OK, passed $testCases tests.")
      case Proved =>
        println(s"+ OK, proved property.")
    }

  val ES: ExecutorService = Executors.newCachedThreadPool
  val pl = Prop.forAll(Gen.unit(Par.unit(1)))(i =>
    Par.map(i)(_ + 1)(ES).get == Par.unit(2)(ES).get
  )

  def check(p: => Boolean): Prop = Prop { (_, _, _) =>
    if (p) Passed else Falsified("()", 0)
  }

  def p2 = Prop.check {
    val p = Par.map(Par.unit(1))(_ + 1)
    val p2 = Par.unit(2)
    p(ES).get == p2(ES).get
  }

  def equal[A](p: Par[A], p2: Par[A]): Par[Boolean] = {
    Par.map2(p, p2)(_ == _)
  }

  def p3 = Prop.check {
    equal(
      Par.map(Par.unit(1))(_ + 1),
      Par.unit(2)
    )(ES).get
  }

  val S = Gen.weighted (
    Gen.choose(1,4).map(Executors.newFixedThreadPool) -> .75,
    Gen.unit(Executors.newCachedThreadPool) -> .25
  )

  def forAllPar[A](g: Gen[A])(f: A => Par[Boolean]): Prop =
    forAll(S.map2(g)((_,_))) { case (s,a) => f(a)(s).get }

  def checkPar(p: Par[Boolean]): Prop =
    forAllPar(Gen.unit(()))(_ => p)

  def forAllPar1[A](g: Gen[A])(f: A => Par[Boolean]): Prop =
    forAll(S ** g) { case (s,a) => f(a)(s).get }

  def forAllPar2[A](g: Gen[A])(f: A => Par[Boolean]): Prop =
    forAll(S ** g) { case s ** a => f(a)(s).get }

  val pint = Gen.choose(0, 10) map (Par.unit(_))
  val p4 =
    forAllPar2(pint)(n => equal(Par.map(n)(y => y), n))

  val forkProp =
    Prop.forAllPar2(Gen.pint2)(i => equal(Par.fork(i), i)) tag "fork"

  val int = Gen.choose(1, 1000)
  val isEven = (i: Int) => i % 2 == 0
  val takeWhileProp =
    Prop.forAll(Gen.listOf(int))(ns => ns.takeWhile(isEven).forall(isEven))

  val takeProp =
    Prop.forAll(Gen.listOf(int))(ns => ns.take(ns.length) == ns)

  val dropProp =
    Prop.forAll(Gen.listOf(int))(ns => ns.drop(ns.length).isEmpty)

//  val unfoldProp =
//    Prop.forAll(Gen.listOf(int))(ns => unfold(ns)(ns => Some((ns, _)))
}

case class Gen[+A](sample: State[RNG,A]) {
  def map[B](f: A => B): Gen[B] =
    Gen(sample.map(f))

  def map2[B,C](g: Gen[B])(f: (A,B) => C): Gen[C] =
    Gen(sample.map2(g.sample)(f))

  def flatMap[B](f: A => Gen[B]): Gen[B] =
    Gen(sample.flatMap(a => f(a).sample))

  def unsized = SGen[A](_ => this)

  def listOfN(size: Int): Gen[List[A]] =
    Gen.listOfN(size, this)

  def listOfN(size: Gen[Int]): Gen[List[A]] =
    size flatMap (n => this.listOfN(n))

  def listOf: SGen[List[A]] =
    Gen.listOf(this)

  def listOf1: SGen[List[A]] =
    Gen.listOf1(this)

  def **[B](g: Gen[B]): Gen[(A,B)] =
    (this map2 g)((_,_))
}

object Gen {
  def unit[A](a: => A): Gen[A] =
    Gen(State(RNG.unit(a)))

  def boolean: Gen[Boolean] =
    Gen(State(RNG.boolean))

  def listOf[A](g: Gen[A]): SGen[List[A]] =
    SGen(n => g.listOfN(n))

  def listOfN[A](n: Int, g: Gen[A]): Gen[List[A]] =
    Gen(State.sequence(List.fill(n)(g.sample)))

  def listOf1[A](g: Gen[A]): SGen[List[A]] =
    SGen(n => g.listOfN(n max 1))

  def choose(start: Int, stopExclusive: Int): Gen[Int] = {
    Gen(State(RNG.notNegativeInt).map(n =>  start + n % (stopExclusive - start)))
  }

  def union[A](g1: Gen[A], g2: Gen[A]): Gen[A] =
    boolean flatMap { v => if (v) g1 else g2 }

  def weighted[A](g1: (Gen[A],Double), g2: (Gen[A],Double)): Gen[A] = {
    val g1Threshold = g1._2.abs / (g1._2.abs + g2._2.abs)

    Gen(State(RNG.double).flatMap(v => if (v > g1Threshold) g1._1.sample else g2._1.sample))
  }

  //implicit def unsized[A](g: Gen[A]): SGen[A] = SGen(_ => g)

  val smallInt = Gen.choose(-10, 10)
  val maxProp = forAll(listOf1(smallInt)) { l =>
    val max = l.max
    !l.exists(_ > max)
  }
  val orderedProp = forAll(listOf1(smallInt)) { l =>
    val sortedList = l.sorted
    isOrdered(sortedList)
  }

  private def isOrdered(l: List[Int]) =
    l.foldLeft((true, None:Option[Int]))((x,y) =>
      (x._1 && x._2.map(_ <= y).getOrElse(true), Some(y)))._1

  object ** {
    def unapply[A,B](p: (A,B)) = Some(p)
  }

  lazy val pint2: Gen[Par[Int]] = Gen.choose(-100,100).listOfN(choose(0, 20)).map(l =>
    l.foldLeft(Par.unit(0))((p,i) =>
      Par.fork { Par.map2(p, Par.unit(i))(_ + _) }
    )
  )

  def genStringIntFn(g: Gen[Int]): Gen[String => Int] =
    g map (i => (s => i))

}

case class SGen[+A](forSize: Int => Gen[A]) {
  def apply(n: Int): Gen[A] =
    forSize(n)
  def map[B](f: A => B): SGen[B] =
    SGen { forSize(_) map f }
  def flatMap[B](f: A => SGen[B]): SGen[B] = {
    val v1: Int => Gen[B] = n => {
      forSize(n) flatMap { i => f(i).forSize(n) }
    }
    SGen(v1)
  }

  def **[B](s2: SGen[B]): SGen[(A,B)] =
    SGen(n => apply(n) ** s2(n))
}


object ExampleUsage extends App {

  //Prop.run(p = Gen.maxProp)
  Prop.run(p = Gen.maxProp)
}
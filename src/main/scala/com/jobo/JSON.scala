package com.jobo

import com.jobo.ReferenceTypes.{Failed, Parser, Success}

import scala.language.higherKinds
import scala.language.implicitConversions
import ReferenceTypes._
import scala.util.matching.Regex

sealed trait JSON
object JSON {
  case object JNull extends JSON
  case class JNumber(get: Double) extends JSON
  case class JString(get: String) extends JSON
  case class JBoolean(get: Boolean) extends JSON
  case class JArray(get: IndexedSeq[JSON]) extends JSON
  case class JObject(get: Map[String, JSON]) extends JSON

  def jsonParser[Parser[+_]](P: Parsers[Parser]): Parser[JSON] = {
    import P._
    implicit def tok(s: String) = token(P.string(s))

    def array = surround(string("[") ,string("]"))(value sep string(",") map { vs => JArray(vs.toIndexedSeq) }) scope "array"

    def obj = surround(string("{"), string("}"))(keyval sep string(",") map { kvs => JObject(kvs.toMap) }) scope "object"

    def keyval = escapedQuoted ** (string(":") *> value)

    def lit = scope("literal") {
      string("null").as(JNull) |
      double.map(JNumber) |
      escapedQuoted.map(JString) |
      string("true").as(JBoolean(true)) |
      string("false").as(JBoolean(false))
    }

    def value: Parser[JSON] = lit | obj | array

    root(whitespace *> (obj | array))
  }
}

object JSONParserApp extends App {
  val testJson =
    """
      {
        "this": "alsś",
        "isActive":"true"
      }
    """.stripMargin

  JSON.jsonParser(Reference)
}

object ReferenceTypes {
  type Parser[+A] = Location => Result[A]

  case class ParseState(loc: Location) {
    def advancedBy(numChar: Int): ParseState =
      copy(loc = loc.copy(offset = loc.offset + numChar))
  }

  sealed trait Result[+A] {

  }

  case class Success[A](get: A, charterConsumed: Int) extends Result[A]
  case class Failed[A](get: ParseError) extends Result[Nothing]
}

object Reference extends Parsers[Parser] {
  override def run[A](p: Parser[A])(input: String): Either[ParseError, A] = ???

  def string(s: String): Parser[String] =
    loc => {
      val idx = findFirstNonMatchIndex(loc.input, s, loc.offset)
      if (idx == -1)
        Success(s, s.length)
      else
        Failed(loc.advanceBy(idx).toError(s"'Expected: $s'"))
    }

  private def findFirstNonMatchIndex(in: String, in2: String, offset: Int): Int = {
    // TODO: Please check this
    in.zip(in2).takeWhile(Function.tupled(_ == _)).unzip._1.size match {
      case size if size == offset => -1
      case size => size
    }
  }

  def regex(r: Regex): Parser[String] =
    loc => {
      r.findPrefixOf(loc.input) match {
        case None => Failed(loc.toError(s"regex: $r"))
        case Some(m) => Success(m, m.length)
      }
    }

  def succeed[A](a: A): Parser[A] =
    s => Success(a, 0)

  def slice[A](p: Parser[A]): Parser[String] = ???
  //  s => p(s) match {
  //    case Success(_,n) => Success(s.slice(n), n)
  //    case fail @ Failed(_) => fail
  //  }

  override def many1[A](p: Parser[A]): Parser[List[A]] = ???

  override def orString(s1: String, s2: String): Parser[String] = ???

  override def or[A](s1: Parser[A], s2: Parser[A]): Parser[A] = ???

  override def flatMap[A, B](p: Parser[A])(f: (A) => Parser[B]): Parser[B] = ???

  override def label[A](msg: String)(p: Parser[A]): Parser[A] = ???

  override def scope[A](msg: String)(p: Parser[A]): Parser[A] = ???

  override def attempt[A](p: Parser[A]): Parser[A] = ???
}

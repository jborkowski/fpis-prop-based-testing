def streamRange(from: Int, to: Int): Stream[Int] =
  if (from >= to) Stream.empty
  else Stream.cons(from, streamRange(from + 1, to))

def listRange(from: Int, to: Int): List[Int] =
  (from until to).toList

def isPrime(v: Int): Boolean =
  (2 until v).forall(e => v%e != 0)

listRange(1000, 10000) filter isPrime

(streamRange(1000, 10000) filter isPrime) apply 1

def from(n: Int): Stream[Int] = n #:: from(n +  1)

def sqrtStream(n: Double): Stream[Double] = {
  def improve(guess: Double): Double = (guess + n / guess) / 2
  lazy val guesses: Stream[Double] = 1 #:: (guesses map improve)
  guesses
}

def isGoodEnough(guess: Double, x: Double): Boolean = math.abs((guess * guess - x) / x) < 0.0001

sqrtStream(4).filter(isGoodEnough(_, 4)).toList
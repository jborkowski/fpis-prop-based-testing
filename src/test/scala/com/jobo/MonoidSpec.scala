package com.jobo

import org.scalatest._

class MonoidSpec extends FlatSpec with Matchers {
  "A method" should "return true when input list is ordered" in {

   val input = IndexedSeq(1,2,4,5,7)
   val input2 = IndexedSeq(7,1,2,4,5)

   Monoid.ordered(input) should be (true)
   Monoid.ordered(input2) should not be (true)
  }

  "Word Counter" should "count words in provided string" in {
    val input = "All by some"
    Monoid.count(input) should be (3)
  }
}

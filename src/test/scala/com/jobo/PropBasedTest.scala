//package com.jobo
//
//import org.scalacheck.{Gen, Properties}
//import org.scalacheck.Prop.forAll
//
//object PropBasedTest extends Properties("test-test") {
//  val intList = Gen.listOf(Gen.choose(0, 100))
//
//  val prop = forAll(intList) { ns =>
//    ns.reverse.reverse == ns
//  } && forAll(intList) { ns =>
//    ns.headOption == ns.reverse.lastOption
//  }
//
//  val failingProp = forAll(intList) { ns =>
//    ns.reverse == ns
//  }
//
//  // prop.check
//
//  // failing test !
//  // failingProp.check
//
//
//
//  // Exercise 8.1
//
//  val exProp = forAll(intList) { ns =>
//    ns.reverse.sum == ns.sum
//  } && forAll(intList) { ns =>
//    val len = ns.length
//    if (ns.distinct.length == 1) {
//      ns.sum == ns.head * len
//    } else {
//      true
//    }
//  }
//
//
//  exProp.check
//}
